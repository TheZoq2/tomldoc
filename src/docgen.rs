use std::collections::{HashMap, HashSet};

use color_eyre::{
    eyre::{anyhow, bail, Context},
    Result,
};
use lazy_static::lazy_static;
use log::{debug, error, warn};
use regex::Regex;
use rustdoc_types::{
    Crate, GenericArg, GenericArgs, Generics, Id, ItemEnum, ItemSummary, StructKind, Type,
};

use crate::{
    documentation::{
        DocumentedName, FieldKind, StructDoc, StructField, TypeDoc, TypeDocKind, TypeLink,
    },
    enum_doc::{EnumVariant, TaggedEnumDoc, UntaggedEnum},
};

pub trait NameExt {
    fn or_unknown(&self) -> String;
    fn check_rename(&self, attrs: &Vec<String>) -> Option<String>;
}
impl NameExt for Option<String> {
    fn or_unknown(&self) -> String {
        self.as_ref()
            .map(|s| s.clone())
            .unwrap_or_else(|| String::from("<unknown>"))
    }

    fn check_rename(&self, attrs: &Vec<String>) -> Option<String> {
        let renamed = attrs
            .iter()
            .find_map(|attr| {
                lazy_static! {
                    static ref RE: Regex =
                        Regex::new(r#"#\[serde\(rename *= *"([^"]*)"\)\]"#).unwrap();
                }
                RE.captures(attr)
            })
            .and_then(|c| c.get(1))
            .map(|name| name.as_str().to_string());

        match (renamed, self) {
            (Some(r), _) => Some(r),
            (None, Some(s)) => Some(s.clone()),
            (None, None) => None,
        }
    }
}

struct ItemQueue {
    seen: HashSet<(Id, Vec<Type>)>,
    queue: Vec<(Id, Vec<Type>)>,
}

impl ItemQueue {
    pub fn from_vec(x: Vec<(Id, Vec<Type>)>) -> Self {
        Self {
            seen: x.iter().cloned().collect(),
            queue: x,
        }
    }
    pub fn push(&mut self, id: (Id, Vec<Type>)) {
        if !self.seen.contains(&id) {
            self.seen.insert(id.clone());
            self.queue.push(id)
        }
    }

    pub fn pop(&mut self) -> Option<(Id, Vec<Type>)> {
        self.queue.pop()
    }
}

fn find_id_of_path(doc: &Crate, path: &[String], in_item: &Id) -> Result<Id> {
    if path.is_empty() {
        Ok(in_item.clone())
    } else {
        let item = &doc.index[in_item];
        match &item.inner {
            ItemEnum::Module(m) => find_id_of_path(
                doc,
                &path[1..],
                m.items
                    .iter()
                    .find(|id| {
                        let inner = &doc.index[id];
                        inner.name.as_ref() == Some(&path[0])
                    })
                    .ok_or_else(|| anyhow!("No item named `{}`", path[0]))
                    .with_context(|| format!("In module `{}`", item.name.or_unknown()))?,
            ),
            ItemEnum::ExternCrate { .. } => {
                bail!("ExternCrate not supported")
            }
            ItemEnum::Use(_) => bail!("Use is not supported"),
            i @ ItemEnum::Union(_)
            | i @ ItemEnum::Enum(_)
            | i @ ItemEnum::Variant(_)
            | i @ ItemEnum::Function(_)
            | i @ ItemEnum::Trait(_)
            | i @ ItemEnum::TraitAlias(_)
            | i @ ItemEnum::Impl(_)
            | i @ ItemEnum::TypeAlias(_)
            | i @ ItemEnum::Static(_)
            | i @ ItemEnum::Macro(_)
            | i @ ItemEnum::ProcMacro(_)
            | i @ ItemEnum::ExternType
            | i @ ItemEnum::Primitive(_)
            | i @ ItemEnum::Constant { .. }
            | i @ ItemEnum::AssocConst { .. }
            | i @ ItemEnum::AssocType { .. }
            | i @ ItemEnum::Struct(_)
            | i @ ItemEnum::StructField(_) => {
                bail!(
                    "Looking for sup-path of {i:?}. Remaining path {}",
                    path.join("::")
                )
            }
        }
    }
}

/// Resolves generic types assuming no types are generic as should be the case for
/// configuiration structs with Vec, Option etc.
fn resolve_generics(args: &GenericArgs) -> Result<Vec<Type>> {
    match args {
        GenericArgs::AngleBracketed { args, .. } => Ok(args
            .iter()
            .map(|a| match &a {
                GenericArg::Type(ty) => Ok(vec![ty.clone()]),
                GenericArg::Const(_) => {
                    bail!("Const types are unsupported")
                }
                GenericArg::Lifetime(_) => Ok(vec![]),
                GenericArg::Infer => {
                    bail!("Generic infer is unsupported")
                }
            })
            .collect::<Result<Vec<_>>>()?
            .into_iter()
            .flatten()
            .collect()),
        GenericArgs::Parenthesized { .. } => {
            bail!("Parenthesized types are not supported")
        }
    }
}

fn resolve_generic_names(doc: &Crate, types: &[Type]) -> Vec<ItemSummary> {
    types
        .iter()
        .filter_map(|p| match p {
            Type::ResolvedPath(p) => Some(doc.paths[&p.id].clone()),
            s @ Type::DynTrait(_)
            | s @ Type::Generic(_)
            | s @ Type::Primitive(_)
            | s @ Type::FunctionPointer(_)
            | s @ Type::Pat { .. }
            | s @ Type::Tuple(_)
            | s @ Type::Slice(_)
            | s @ Type::Array { .. }
            | s @ Type::ImplTrait(_)
            | s @ Type::Infer
            | s @ Type::RawPointer { .. }
            | s @ Type::BorrowedRef { .. }
            | s @ Type::QualifiedPath { .. } => {
                warn!("Generic bindings of type {s:?} are unsupported");
                None
            }
        })
        .collect()
}

fn field_type_doc(
    doc: &Crate,
    ty: &Type,
    item_queue: &mut ItemQueue,
    generic_map: &HashMap<String, Type>,
) -> Result<FieldKind> {
    match ty {
        Type::ResolvedPath(path) => {
            // We'll special-case some builtin types here. Like vecs, maps and options
            let path_full = &doc.paths[&path.id].path;
            match path_full
                .iter()
                .map(|s| s.as_ref())
                .collect::<Vec<_>>()
                .as_slice()
            {
                ["core", "option", "Option"] => {
                    let ty = resolve_generics(path.args.as_ref().unwrap())
                        .and_then(|args| field_type_doc(doc, &args[0], item_queue, generic_map))
                        .context("when resolving std::option::Option type parameters")?;

                    Ok(FieldKind::Optional(Box::new(ty)))
                }
                ["alloc", "vec", "Vec"] => {
                    let ty = resolve_generics(path.args.as_ref().unwrap())
                        .and_then(|args| field_type_doc(doc, &args[0], item_queue, generic_map))
                        .context("when resolving std::vec::Vec type parameters")?;

                    Ok(FieldKind::List(Box::new(ty)))
                }
                ["std", "collections", "hash", "set", "HashSet"] => {
                    let ty = resolve_generics(path.args.as_ref().unwrap())
                        .and_then(|args| field_type_doc(doc, &args[0], item_queue, generic_map))
                        .context("when resolving HashSet type parameters")?;

                    Ok(FieldKind::Set(Box::new(ty)))
                }
                ["std", "collections", "hash", "map", "HashMap"] => {
                    let from_ty = resolve_generics(path.args.as_ref().unwrap())
                        .and_then(|args| field_type_doc(doc, &args[0], item_queue, generic_map))
                        .context("when resolving std::vec::Vec type parameters")?;
                    let to_ty = resolve_generics(path.args.as_ref().unwrap())
                        .and_then(|args| field_type_doc(doc, &args[1], item_queue, generic_map))
                        .context("when resolving std::vec::Vec type parameters")?;

                    Ok(FieldKind::Map(Box::new(from_ty), Box::new(to_ty)))
                }
                ["std", "path", "PathBuf"]
                | ["std", "path", "Path"]
                | ["camino", "Utf8PathBuf"]
                | ["camino", "Utf8Path"] => Ok(FieldKind::Primitive("FilePath".to_string())),
                ["alloc", "string", "String"] => Ok(FieldKind::Primitive("String".to_string())),
                other => {
                    if matches!(other, ["std", ..] | ["core", ..] | ["alloc", ..]) {
                        warn!("Unknown builtin type {}", path_full.join("::"));
                    }
                    let generics = path
                        .args
                        .as_ref()
                        .map(|a| resolve_generics(a))
                        .transpose()?
                        .unwrap_or(vec![]);

                    item_queue.push((path.id.clone(), generics.clone()));
                    let id = path.id.clone();
                    let link = TypeLink {
                        item_summary: DocumentedName {
                            base: doc.paths[&id].clone(),
                            generic_binds: resolve_generic_names(doc, &generics),
                        },
                    };
                    Ok(FieldKind::Other(link))
                }
            }
        }
        Type::Primitive(name) if name == "bool" => Ok(FieldKind::Bool),
        Type::Primitive(name) => Ok(FieldKind::Primitive(name.clone())),
        // Componund types which I intend to support but are not ready yet
        Type::Array { .. } | Type::Tuple(_) | Type::Slice(_) | Type::Pat { .. }=> {
            bail!("Type {ty:?} not currently supported")
        }
        Type::Generic(name) => {
            if let Some(ty) = generic_map.get(name) {
                field_type_doc(doc, ty, item_queue, generic_map)
            } else {
                bail!("Did not find a generic map entry for {name}")
            }
        }
        Type::BorrowedRef {
            ..
        } => {
            bail!("References not currently supported")
        }
        // Types which probably won't be supported
        Type::DynTrait(_)
        | Type::FunctionPointer(_)
        | Type::ImplTrait(_)
        | Type::Infer
        | Type::RawPointer { .. }
        | Type::QualifiedPath { .. } => {
            bail!("Type {ty:?} not supported")
        }
    }
}

fn get_field_documentation(
    doc: &Crate,
    field_id: &Id,
    item_queue: &mut ItemQueue,
    generic_map: &HashMap<String, Type>,
) -> Option<StructField> {
    let item = &doc.index[field_id];

    if let ItemEnum::StructField(inner) = &item.inner {
        let skip = item.attrs.iter().any(|attr| {
            debug!("Found field attr {attr}");

            attr == "#[serde(skip)]" || attr == "#[serde(skip_deserializing)]"
        });

        if skip {
            None
        } else {
            Some(StructField {
                name: item.name.check_rename(&item.attrs).or_unknown(),
                doc: item.docs.clone(),
                type_doc: field_type_doc(doc, &inner, item_queue, generic_map)
                    .with_context(|| format!("In field {}", item.name.or_unknown()))
                    .unwrap_or_else(|e| FieldKind::Error(format!("{:#?}", e))),
            })
        }
    } else {
        panic!("Did not find docs for field with ID {field_id:?}")
    }
}

// Used for tuple enum generation where structs are transparent. Resolves a struct field to a type,
// then documents that type
fn document_field_as_type(
    doc: &Crate,
    field_id: &Id,
    item_queue: &mut ItemQueue,
) -> Result<Option<TypeDocKind>> {
    let item = &doc.index[field_id];

    if let ItemEnum::StructField(ty) = &item.inner {
        match ty {
            Type::ResolvedPath(t) => Ok(Some(generate_documentation(
                doc,
                &(
                    t.id.clone(),
                    t.args
                        .clone()
                        .map(|g| resolve_generics(&g))
                        .transpose()?
                        .unwrap_or(vec![]),
                ),
                item_queue,
            ))),
            _ => {
                warn!("Enum variant with a non-struct field is not supported");
                Ok(None)
            }
        }
    } else {
        panic!("Did not find docs for field with ID {field_id:?}")
    }
}

fn generate_enum_variant(
    doc: &Crate,
    variant_id: &Id,
    item_queue: &mut ItemQueue,
    generic_map: &HashMap<String, Type>,
) -> Result<EnumVariant> {
    let item = &doc.index[variant_id];

    if let ItemEnum::Variant(inner) = &item.inner {
        let fields = match &inner.kind {
            rustdoc_types::VariantKind::Plain => {
                vec![]
            }
            rustdoc_types::VariantKind::Tuple(fields) => {
                // An enum like this inlines the struct fields of its only variant, i.e.
                // ```rust
                // #[derive(Deserialize, Clone, Debug)]
                // #[serde(tag = "architecture")]
                // pub enum Pnr {
                //     #[serde(rename = "ice40")]
                //     Ice40(NextPnrArgs<Ice40Device>),
                //     #[serde(rename = "ecp5")]
                //     Ecp5(NextPnrArgs<Ecp5Device>),
                // }
                // ```

                match fields.as_slice() {
                    [Some(id)] => {
                        let type_doc = document_field_as_type(doc, id, item_queue)?;
                        match type_doc {
                            Some(TypeDocKind::Struct(s)) => s.fields,
                            Some(_) => {
                                warn!(
                                    "Tuple enums with more than one option are not supported. Needed by {}",
                                    item.name.or_unknown()
                                );
                                vec![]
                            }
                            None => vec![],
                        }
                    }
                    _ => {
                        warn!(
                            "Tuple enums with more than one option are not supported. Needed by {}",
                            item.name.or_unknown()
                        );
                        vec![]
                    }
                }
            }
            rustdoc_types::VariantKind::Struct {
                fields,
                ..
            } => fields
                .iter()
                .filter_map(|f| get_field_documentation(doc, &f, item_queue, generic_map))
                .collect(),
        };
        Ok(EnumVariant {
            name: item.name.check_rename(&item.attrs).or_unknown(),
            doc: item.docs.clone(),
            fields,
        })
    } else {
        bail!("Did not find docs for variant with ID {variant_id:?}")
    }
}

fn get_generic_map(generics: &Generics, generic_bindings: &[Type]) -> HashMap<String, Type> {
    generics
        .params
        .iter()
        .filter(|g| match &g.kind {
            rustdoc_types::GenericParamDefKind::Lifetime { .. } => false,
            rustdoc_types::GenericParamDefKind::Type { .. } => true,
            rustdoc_types::GenericParamDefKind::Const { .. } => false,
        })
        .zip(generic_bindings)
        .map(|(g, t)| (g.name.clone(), t.clone()))
        .collect::<HashMap<_, _>>()
}

/// Returns a vector of things that need documentation in addition to the specified item.
/// For example, a struct with a sub-struct as a field requires documentation for its
/// subfields.
fn generate_documentation(
    doc: &Crate,
    item: &(Id, Vec<Type>),
    item_queue: &mut ItemQueue,
) -> TypeDocKind {
    let id = &item.0;
    let generic_bindings = item.1.clone();
    let Some(item) = &doc.index.get(id) else {
        return TypeDocKind::Error(format!("Did not find type with id {item:?}"));
    };

    match &item.inner {
        // We can't document modules, so we'll ignore them
        ItemEnum::Union(_) => todo!(),
        ItemEnum::Struct(s) => match &s.kind {
            StructKind::Unit => TypeDocKind::Error("Unit structs not supported".to_string()),
            StructKind::Tuple(_) => TypeDocKind::Error("Tuple structs not supported".to_string()),
            StructKind::Plain {
                fields,
                ..
            } => {
                let generic_map = get_generic_map(&s.generics, &generic_bindings);

                let field_docs = fields
                    .iter()
                    .filter_map(|f| get_field_documentation(doc, &f, item_queue, &generic_map))
                    .collect();

                TypeDocKind::Struct(StructDoc {
                    name: item.name.check_rename(&item.attrs).or_unknown(),
                    doc: item.docs.clone(),
                    fields: field_docs,
                })
            }
        },
        ItemEnum::StructField(_) => {
            unreachable!("Struct fields should be handled by get_field_documentation")
        }
        ItemEnum::Enum(inner) => {
            let generic_map = get_generic_map(&inner.generics, &generic_bindings);

            let tag = item
                .attrs
                .iter()
                .find_map(|attr| {
                    lazy_static! {
                        static ref RE: Regex =
                            Regex::new(r#"#\[serde\(tag *= *"([^"]*)"\)\]"#).unwrap();
                    }

                    RE.captures(attr)
                })
                .and_then(|c| c.get(1))
                .map(|c| c.as_str().to_string());

            let is_untagged = item.attrs.iter().any(|attr| attr == "#[serde(untagged)]");

            let variants = inner
                .variants
                .iter()
                .map(
                    |v| match generate_enum_variant(doc, v, item_queue, &generic_map) {
                        Ok(variant) => variant,
                        Err(e) => {
                            error!("{e:#?}");
                            EnumVariant {
                                name: "error".to_string(),
                                doc: None,
                                fields: vec![],
                            }
                        }
                    },
                )
                .collect::<_>();

            if let Some(tag) = tag {
                let tagged_enum = TaggedEnumDoc {
                    name: item.name.check_rename(&item.attrs).or_unknown(),
                    doc: item.docs.clone(),
                    variants,
                    tag: tag.to_string(),
                };
                TypeDocKind::TaggedEnum(tagged_enum)
            } else if is_untagged {
                TypeDocKind::UntaggedEnum(UntaggedEnum {
                    name: item.name.check_rename(&item.attrs).or_unknown(),
                    doc: item.docs.clone(),
                    variants,
                })
            } else {
                for variant in &variants {
                    if !variant.fields.is_empty() {
                        warn!("In {}", doc.paths[id].path.join("::"));
                        warn!("Externally tagged enums with payload are not supported.");
                        warn!("Documentation will be generated anyway but will not include");
                        warn!("fields.");
                    }
                }

                TypeDocKind::SimpleEnum(crate::enum_doc::SimpleEnum {
                    name: item.name.check_rename(&item.attrs).or_unknown(),
                    doc: item.docs.clone(),
                    variants,
                })
            }
        }
        ItemEnum::Variant(_) => todo!("Enum variant documentation should be handled elsewhere"),
        ItemEnum::TypeAlias(_) => todo!("Handle type aliases"),
        ItemEnum::Primitive(_) => todo!(),
        ItemEnum::Function(_)
        | ItemEnum::Trait(_)
        | ItemEnum::TraitAlias(_)
        | ItemEnum::Impl(_)
        | ItemEnum::Constant{..}
        | ItemEnum::Static(_)
        | ItemEnum::Macro(_)
        | ItemEnum::ProcMacro(_)
        | ItemEnum::AssocConst { .. }
        | ItemEnum::AssocType { .. }
        | ItemEnum::Module(_)
        | ItemEnum::ExternCrate { .. }
        | ItemEnum::ExternType
        | ItemEnum::Use(_) => TypeDocKind::Error(format!("{item:?} structs not supported")),
    }
}

pub fn generate_docs(doc: &Crate, doc_item: &Vec<String>) -> Result<Vec<TypeDoc>> {
    // Vec of IDs to document
    let mut queue = ItemQueue::from_vec(vec![(find_id_of_path(doc, doc_item, &doc.root)?, vec![])]);

    let mut docs = vec![];
    while let Some(item) = queue.pop() {
        let self_path = doc.paths[&item.0].clone();

        let generic_summaries = resolve_generic_names(doc, &item.1);

        docs.push(TypeDoc {
            paths: DocumentedName {
                base: self_path,
                generic_binds: generic_summaries,
            },
            doc: generate_documentation(doc, &item, &mut queue),
        });
    }

    Ok(docs)
}
