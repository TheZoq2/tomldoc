use itertools::Itertools;
use nesty::{code, Code};

use crate::documentation::{StructField, TypeDocumentation};

pub struct EnumVariant {
    pub name: String,
    pub doc: Option<String>,
    pub fields: Vec<StructField>,
}

pub struct UntaggedEnum {
    pub name: String,
    pub doc: Option<String>,
    pub variants: Vec<EnumVariant>,
}

fn details(variant: &EnumVariant, extra_fields: &[(&str, &str)]) -> Code {
    code! {
        [0] format!("#### {}", variant.name);
        [0] variant.doc.clone();
        [0] "```toml";
        [0] extra_fields.iter().map(|(tag, value)| {
            format!(r#"{} = "{}""#, tag, value)
        }).join("\n");
        [0] variant.fields.iter().map(|f| {
            f.example().to_string()
        }).join("\n");
        [0] "```";
        [0] "<details class=enum_field_docs>";
        [0] "";
        [0] "<summary>Fields</summary>";
        [0] "";
        [0] variant.fields.iter().map(|f| {
            f.docs("h4", "enum_field").to_string()
        }).join("\n");
        [0] "";
        [0] "</details>";
        [0] "";
    }
}

impl TypeDocumentation for UntaggedEnum {
    fn title(&self) -> String {
        self.name.clone()
    }

    fn self_doc(&self) -> Code {
        code! {
            [0] self.doc.clone()
        }
    }

    fn example(&self) -> Code {
        code! {}
    }

    fn details(&self) -> Code {
        let examples = self
            .variants
            .iter()
            .map(|v| details(v, &[]))
            .collect::<Vec<_>>();

        code! {
            [0] "### One of the following:";
            [0] examples
        }
    }
}

pub struct TaggedEnumDoc {
    pub name: String,
    pub doc: Option<String>,
    pub tag: String,
    pub variants: Vec<EnumVariant>,
}

impl TypeDocumentation for TaggedEnumDoc {
    fn title(&self) -> String {
        self.name.clone()
    }

    fn self_doc(&self) -> Code {
        code! {
            [0] self.doc.clone()
        }
    }

    fn example(&self) -> Code {
        code! {}
    }

    fn details(&self) -> Code {
        let examples = self
            .variants
            .iter()
            .map(|v| details(v, &[(&self.tag, &format!(r#"{}"#, v.name))]))
            .collect::<Vec<_>>();

        code! {
            [0] "### One of the following:";
            [0] examples
        }
    }
}

pub struct SimpleEnum {
    pub name: String,
    pub doc: Option<String>,
    pub variants: Vec<EnumVariant>,
}

impl TypeDocumentation for SimpleEnum {
    fn title(&self) -> String {
        self.name.clone()
    }

    fn self_doc(&self) -> Code {
        code! {
            [0] self.doc.clone()
        }
    }

    fn example(&self) -> Code {
        code! {}
    }

    fn details(&self) -> Code {
        code! {
            [0] "### One of these strings:";
            [0] self.variants.iter().map(|v| {
                code! {
                    [0] format!(r#"- `"{}"`"#, v.name);
                    [1]     &v.doc;
                }.to_string()
            }).join("\n");
        }
    }
}
