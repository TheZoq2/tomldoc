mod docgen;
mod documentation;
mod enum_doc;

use camino::Utf8PathBuf;
use clap::Parser;
use color_eyre::{
    eyre::{bail, Context},
    Result,
};
use itertools::Itertools;
use log::{info, LevelFilter};
use serde::{Deserialize, Serialize};

#[derive(Parser)]
struct Args {
    #[clap(long, short)]
    output_dir: Utf8PathBuf,
}

#[derive(Serialize, Deserialize)]
struct Config {
    /// The name of the crate which is being documented
    crate_name: String,
    /// Items to document as fully qualified rust paths relative to the current crate
    doc_items: Vec<Vec<String>>,
}

#[tokio::main]
async fn main() -> Result<()> {
    env_logger::Builder::from_env("TOMLDOC_LOG")
        .filter_level(LevelFilter::Info)
        .format_timestamp(None)
        .init();
    color_eyre::install()?;

    let args = Args::parse();

    if !args.output_dir.exists() {
        tokio::fs::create_dir_all(&args.output_dir)
            .await
            .with_context(|| format!("Failed to create {}", args.output_dir))?;
    }

    info!("Reading config");
    let config_str = tokio::fs::read_to_string("tomldoc.toml")
        .await
        .context("Failed to read tomldoc.toml")?;

    info!("Parsing config");
    let config = toml::from_str::<Config>(&config_str).context("Failed to decode tomldoc.toml")?;

    let doc_json_path = Utf8PathBuf::from("target/doc/")
        .join(config.crate_name)
        .with_extension("json");

    info!("Running rustdoc");
    let status = tokio::process::Command::new("cargo")
        .args([
            "+nightly",
            "rustdoc",
            "--",
            "-Z",
            "unstable-options",
            "--output-format",
            "json",
            "--document-private-items",
        ])
        .status()
        .await
        .context("Failed to run cargo +nightly rustdoc")?;

    if !status.success() {
        bail!("cargo doc failed");
    }

    info!("Reading {doc_json_path}");
    let doc_json_content = tokio::fs::read_to_string(&doc_json_path)
        .await
        .with_context(|| format!("Failed to read {doc_json_path}"))?;

    let doc = serde_json::from_str(&doc_json_content)
        .with_context(|| format!("Failed to decode {doc_json_path} as json"))?;

    for item in config.doc_items {
        let docs = docgen::generate_docs(&doc, &item)?;

        let content = docs
            .iter()
            .enumerate()
            .map(|(i, doc)| {
                let start_open = i == 0;
                doc.get_toml(start_open)
            })
            .join("\n");

        let filename = args.output_dir.join(item.join("__")).with_extension("md");
        tokio::fs::write(&filename, content)
            .await
            .with_context(|| filename)?;
    }

    Ok(())
}
