use std::cmp::Ordering;

use itertools::Itertools;
use nesty::{code, Code};
use rustdoc_types::ItemSummary;

use crate::enum_doc::{SimpleEnum, TaggedEnumDoc, UntaggedEnum};

pub trait SummaryExt {
    fn name(&self) -> String;
    fn markdown_id(&self) -> String;
}

impl SummaryExt for DocumentedName {
    fn name(&self) -> String {
        let base = self.base.path.last().unwrap().clone();
        let generic_args = if !self.generic_binds.is_empty() {
            format!(
                "\\<{}\\>",
                self.generic_binds
                    .iter()
                    .map(|b| { b.path.last().unwrap() })
                    .join(", ")
            )
        } else {
            String::new()
        };
        format!("{base}{}", generic_args)
    }
    fn markdown_id(&self) -> String {
        let base = self.base.path.last().unwrap().clone();
        let generic_args = if !self.generic_binds.is_empty() {
            format!(
                "\\<{}\\>",
                self.generic_binds
                    .iter()
                    .map(|b| { b.path.iter().join("__") })
                    .join(",")
            )
        } else {
            String::new()
        };
        format!("{base}{}", generic_args)
    }
}

pub struct TypeLink {
    pub item_summary: DocumentedName,
}

pub enum FieldKind {
    Primitive(String),
    Bool,
    Other(TypeLink),
    Optional(Box<FieldKind>),
    List(Box<FieldKind>),
    Set(Box<FieldKind>),
    Map(Box<FieldKind>, Box<FieldKind>),
    Error(String),
}

impl FieldKind {
    fn is_inline(&self) -> bool {
        match self {
            FieldKind::Primitive(_) => true,
            FieldKind::Bool => true,
            FieldKind::Other(_) => false,
            FieldKind::Optional(inner) => inner.is_inline(),
            FieldKind::List(_) => true,
            FieldKind::Set(_) => true,
            FieldKind::Error(_) => true,
            FieldKind::Map(_, _) => true,
        }
    }

    fn notes(&self) -> Option<String> {
        match self {
            FieldKind::Primitive(_) => None,
            FieldKind::Bool => None,
            FieldKind::Other(_) => None,
            FieldKind::Optional(_) => Some("Optional".to_string()),
            FieldKind::List(_) => None,
            FieldKind::Set(_) => None,
            FieldKind::Error(_) => None,
            FieldKind::Map(_, _) => None,
        }
    }

    fn get_toml_type(&self) -> String {
        match self {
            FieldKind::Primitive(p) => format!("{p}"),
            FieldKind::Bool => format!("bool"),
            FieldKind::Other(link) => format!(
                "<a href=\"#{}\">{}</a>",
                link.item_summary.name(),
                link.item_summary.markdown_id()
            ),
            FieldKind::Optional(inner) => format!("{}", inner.get_toml_type()),
            FieldKind::List(inner) => format!("[{}]", inner.get_toml_type()),
            FieldKind::Set(inner) => format!("Set[{}]", inner.get_toml_type()),
            FieldKind::Error(e) => format!("**Error: ** {e}"),
            FieldKind::Map(from, to) => {
                format!("Map[{} =&gt; {}]", from.get_toml_type(), to.get_toml_type())
            }
        }
    }

    fn get_toml_shorthand(&self) -> String {
        match self {
            FieldKind::Primitive(p) => match p.as_str() {
                "String" => r#""…""#.to_string(),
                "FilePath" => r#""path/to/file""#.to_string(),
                _ => "…".to_string(),
            },
            FieldKind::Bool => "true|false".to_string(),
            FieldKind::Other(summary) => format!("<{}>", summary.item_summary.name()),
            FieldKind::Optional(inner) => format!("{}", inner.get_toml_shorthand()),
            FieldKind::List(l) => format!("[{}, …]", l.get_toml_shorthand()),
            FieldKind::Set(l) => format!("[{}, …]", l.get_toml_shorthand()),
            FieldKind::Map(_, to) => format!("{{{}: {}, …}}", "key", to.get_toml_shorthand()),
            FieldKind::Error(_) => format!("# Doc generation error"),
        }
    }
}

pub struct StructField {
    pub name: String,
    pub doc: Option<String>,
    pub type_doc: FieldKind,
}

impl StructField {
    pub fn example(&self) -> Code {
        let StructField {
            name,
            doc: field_doc,
            type_doc,
        } = self;

        let notes = type_doc
            .notes()
            .map(|n| format!(" # {n}"))
            .unwrap_or(String::new());

        code! {
            [0] if type_doc.is_inline() {
                code! {
                    [0] field_doc.as_ref().map(|doc| doc.lines().map(|l| format!("# {l}")).join("\n"));
                    [0] format!("{name} = {}{notes}", type_doc.get_toml_shorthand());
                }
            } else {
                code! {
                    [0] "";
                    [0] field_doc.as_ref().map(|doc| doc.lines().map(|l| format!("# {l}")).join("\n"));
                    [0] format!("[{name}]{notes}");
                    [0] format!("{}", type_doc.get_toml_shorthand());
                }
            }
        }
    }

    pub fn docs(&self, heading_tag: &str, heading_class: &str) -> Code {
        let StructField {
            name,
            doc: field_doc,
            type_doc,
        } = self;

        code! {
            [0] "<div class=field_doc>";
            [0] "";
            [0] format!(r#"<{heading_tag} class={heading_class}> <span class=tomldoc_param_name>{name}</span> <span class=tomldoc_type> {} </span></{heading_tag}>"#, type_doc.get_toml_type());
            [0] "";
            [0] field_doc;
            [0] "";
            [0] "</div>";
            [0] ""
        }
    }
}

pub struct StructDoc {
    pub name: String,
    pub doc: Option<String>,
    pub fields: Vec<StructField>,
}

pub trait TypeDocumentation {
    fn title(&self) -> String;

    fn self_doc(&self) -> Code;

    fn example(&self) -> Code;

    fn details(&self) -> Code;

    fn get_toml_doc(&self, start_open: bool, item_summary: &DocumentedName) -> Code {
        code! {
            [0] format!("<details {}>", if start_open {r#"open"#} else {""});
            [0] "";
            [0] format!(
                r#"<summary id="{}">{}</summary>"#,
                item_summary.markdown_id(),
                self.title()
            );
            [0] "";
            [0] self.self_doc();
            [0] "";
            [0] self.example();
            [0] self.details();
            [0] "";
            [0] "</details>";
        }
    }
}

impl TypeDocumentation for StructDoc {
    fn title(&self) -> String {
        format!("{}", self.name)
    }

    fn self_doc(&self) -> Code {
        code! {
            [0] self.doc.clone()
        }
    }

    fn example(&self) -> Code {
        let field_examples = self
            .fields
            .iter()
            // Make sure that non-inline structs are listed before other structs. Stable sort so things otherwise appear in the same order as the input struct
            .sorted_by(|l, r| {
                if l.type_doc.is_inline() && !r.type_doc.is_inline() {
                    Ordering::Less
                } else if !l.type_doc.is_inline() && r.type_doc.is_inline() {
                    Ordering::Greater
                } else {
                    Ordering::Equal
                }
            })
            .map(|f| f.example().to_string())
            .join("\n");

        code! {
            [0] "## Summary";
            [0] "```toml";
            [0] field_examples;
            [0] "```";
        }
    }

    fn details(&self) -> Code {
        let field_docs = self
            .fields
            .iter()
            .map(|f| f.docs("h3", "struct_field").to_string())
            .join("\n");

        code! {
            [0] field_docs
        }
    }
}

pub enum TypeDocKind {
    Struct(StructDoc),
    TaggedEnum(TaggedEnumDoc),
    UntaggedEnum(UntaggedEnum),
    SimpleEnum(SimpleEnum),
    Error(String),
}

pub struct DocumentedName {
    pub base: ItemSummary,
    pub generic_binds: Vec<ItemSummary>,
}

pub struct TypeDoc {
    pub paths: DocumentedName,
    pub doc: TypeDocKind,
}

impl TypeDoc {
    pub fn get_toml(&self, start_open: bool) -> String {
        let content = match &self.doc {
            TypeDocKind::Struct(s) => s.get_toml_doc(start_open, &self.paths).to_string(),
            TypeDocKind::TaggedEnum(e) => e.get_toml_doc(start_open, &self.paths).to_string(),
            TypeDocKind::UntaggedEnum(e) => e.get_toml_doc(start_open, &self.paths).to_string(),
            TypeDocKind::SimpleEnum(e) => e.get_toml_doc(start_open, &self.paths).to_string(),
            TypeDocKind::Error(e) => format!("{e}"),
        };
        code! {
            [0] "<div class=tomldoc>";
            [0] "";
            [0] format!("{}", content);
            [0] "";
            [0] "</div>";
        }
        .to_string()
    }
}
